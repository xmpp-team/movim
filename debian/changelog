movim (0.17.1-1) unstable; urgency=medium

  * New upstream version.
    + Remove patch to set cookie independent of HTTPS encryption.
      (Applied upstream.)

 -- Dominik George <natureshadow@debian.org>  Mon, 30 Mar 2020 17:10:58 +0200

movim (0.17-1) unstable; urgency=medium

  [ Dominik George ]
  * New upstream version.
    + Remove all patches applied upstream.
    + Update remaining patches.
    + Adjust some paths for new source tree layout.
    + Removes SQLite support.
    + Introduce new public cache directory.
  * Bump Standards-Version (no changes needed).
  * Add patch to set cookie independent of HTTPS encryption.

  [ Thorsten Glaser ]
  * Adjust dependencies for installability.
  * Use PSR-4 autoloader for Movim’s App\ namespace.
  * (Re‑)Build emojis list at package build time.

 -- Dominik George <natureshadow@debian.org>  Mon, 09 Mar 2020 22:07:39 +0100

movim (0.14.1-6) unstable; urgency=medium

  * Team upload.
  * Update composer.json to allow symfony 4.x (Closes: #939190)
  * Modernise packaging (bump debhelper and Policy versions)

 -- Thorsten Glaser <tg@mirbsd.de>  Wed, 20 Nov 2019 02:03:04 +0100

movim (0.14.1-5) unstable; urgency=medium

  [ Thorsten Glaser ]
  * Add patch to fix emojis being replaced by the wrong images.

  [ Dominik George ]
  * Add patch to add correct ACLs to webserver configs. (Closes: #928209)

 -- Dominik George <natureshadow@debian.org>  Wed, 08 May 2019 22:38:32 +0200

movim (0.14.1-4) unstable; urgency=medium

  * Restart movim daemon if it exits. (Closes: #924429)
  * Fix MUC autojoin when used in parallel with other clients. (Closes: #924431)
  * Allow long descriptions of MUC rooms. (Closes: #924432)

 -- Dominik George <natureshadow@debian.org>  Tue, 12 Mar 2019 22:49:08 +0100

movim (0.14.1-3) unstable; urgency=medium

  * Fix bug number in last changelog.

 -- Dominik George <natureshadow@debian.org>  Sat, 23 Feb 2019 17:19:27 +0100

movim (0.14.1-2) unstable; urgency=low

  * Add Russian debconf translation (thanks to Lev Lamberov, closes: #921166).
  * Add Dutch debconf translation (thanks to Frans Spiesschaert, closes: #919303).

 -- Dominik George <natureshadow@debian.org>  Sat, 23 Feb 2019 16:19:58 +0100

movim (0.14.1-1) unstable; urgency=medium

  * New upstream version.
    + Finalises 0.14.1 release.
  * Use dh-apache2 to configure apache.
  * Install nginx configuration snippet.

 -- Dominik George <natureshadow@debian.org>  Mon, 11 Feb 2019 22:26:40 +0100

movim (0.14.1~rc5-1) unstable; urgency=high

  * New upstream version.

 -- Dominik George <natureshadow@debian.org>  Tue, 05 Feb 2019 17:10:06 +0100

movim (0.14.1~rc4-2) unstable; urgency=high

  * Team upload.
  * Manually “auto”load from php-react-promise-stream as well,
    to fix some 500 server errors (pointed out by upstream).

 -- Thorsten Glaser <tg@mirbsd.de>  Fri, 18 Jan 2019 15:27:27 +0100

movim (0.14.1~rc4-1) unstable; urgency=medium

  * New upstream version.
    + Refresh patches, drop patches applied upstream.
  * Add debconf translations.
    + German (thanks to Chris Leick; closes: #917324)
    + French (thanks to Jean-Pierre Giraud; closes: #918697)
    + Portuguese (thanks to Rui Branco; closes: #918614)
  * Bump Standards-Version, no changes needed.

 -- Dominik George <natureshadow@debian.org>  Sun, 13 Jan 2019 12:55:57 +0100

movim (0.14.1~rc2-2) unstable; urgency=high

  * Raise PDO module(s) from Recommends to Depends.
    + piuparts does not install Recommends, and one is needed anyway.
  * Suggest ejabberd and apache2.
  * Remove dbconfig-common from jessie-and-below from dependencies.

 -- Dominik George <natureshadow@debian.org>  Mon, 24 Dec 2018 22:46:38 +0100

movim (0.14.1~rc2-1) unstable; urgency=high

  [ Thorsten Glaser ]

  * Fix installation of OpenSans font CSS override.
  * Correct debconf string mismerge.

  [ Dominik George ]

  * Add missing debconf defaults.
  * Support SQLite3 and MySQL via dbconfig-common. (Closes: #915479, #915480)
    + Add a patch to fix temporary bug in database migrations.
    + Also fixes piuparts tests.
    + Restructure dbconfig-common code.
  * New upstream version.
    + Refresh/drop patches.
    + Add patch to allow a non-writable document root.

 -- Dominik George <natureshadow@debian.org>  Sun, 23 Dec 2018 00:20:53 +0100

movim (0.14.1~rc-1) unstable; urgency=high

  * New upstream version.
    + Compatibility with PHP 7.3.
  * Allow configuration of public URL through debconf.
  * Allow configuration of admin username and password
    through debconf (Closes: #915478).
  * Provide a movim command to wrap daemon.php.

 -- Dominik George <natureshadow@debian.org>  Sun, 16 Dec 2018 21:10:23 +0100

movim (0.14-4) unstable; urgency=high

  * Fix some more issues with autoloading.
  * Add missing dependency to GD.

 -- Dominik George <natureshadow@debian.org>  Tue, 04 Dec 2018 00:42:33 +0100

movim (0.14-3) unstable; urgency=high

  * Fix issues with autoloading.
  * Do not encrypt WebSocket connection (always reverse-proxied).
  * Simplify and update d/copyright.

 -- Dominik George <natureshadow@debian.org>  Mon, 03 Dec 2018 12:18:46 +0100

movim (0.14-2) unstable; urgency=high

  * Allow composer to use fig-cookies 2.0.

 -- Dominik George <natureshadow@debian.org>  Mon, 03 Dec 2018 12:14:28 +0100

movim (0.14-1) unstable; urgency=medium

  * Convert patch headers to DEP-3.
  * Fix syntax error in d/copyright.
  * New upstream version (release).
    + Refresh patches.
    + Drop composer.json patch (applied upstream).

 -- Dominik George <natureshadow@debian.org>  Sun, 02 Dec 2018 14:36:38 +0100

movim (0.14~rc-1) experimental; urgency=low

  * Initial release (Closes: #858367)

 -- Dominik George <natureshadow@debian.org>  Thu, 27 Sep 2018 16:06:35 +0200
