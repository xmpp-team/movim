<?php

require_once('Composer/Autoload/ClassLoader.php');
include_once('/usr/share/php/HTMLPurifier.composer.php');
include_once('/usr/share/php/Illuminate/Support/helpers.php');
include_once('/usr/share/php/GuzzleHttp/Psr7/functions_include.php');
include_once('/usr/share/php/React/Promise/functions_include.php');
include_once('/usr/share/php/React/Promise/Stream/functions_include.php');
include_once('/usr/share/php/React/Promise/Timer/functions.php');

$movim_autoloader = new \Composer\Autoload\ClassLoader();
$movim_autoloader->setUseIncludePath(true);
$movim_autoloader->addPsr4('App\\', '/usr/share/movim/app');
if (get_cfg_var('debian') === 'build')
	$movim_autoloader->add('Movim', dirname(__FILE__) . '/../src');
$movim_autoloader->register();
